﻿using System.Linq;
using Calculation.Interfaces;

namespace Calculation.Implementation
{
	public class Calculator : BaseCalculator, ICalculator
	{
		public Calculator(ILogger logger)
		{
		}

		public int Sum(params int[] numbers)
		{
			var sum = SafeSum(numbers);
			return sum;
		}

		public int Sub(int a, int b)
		{
			return SafeSub(a, b);
		}

		public int Multiply(params int[] numbers)
		{
			if (!numbers.Any())
				return 0;

			return SafeMultiply(numbers);
		}

		public int Div(int a, int b)
		{
			return a / b;
		}
	}
}